<?php
/**
 * Template Name: OUPF Blog Page
 *
 * Selectable from a dropdown menu on the edit page screen.
 */

get_header(); ?>

<div class="site-content" style="width:100%">
	<div class="oupf-header oupf-header-surfing" style="background: url('/wp-content/themes/oupf/assets/img/header-imgs/antarctic-peninsula.jpg') center no-repeat">
    <div class="oupf-header-content">

    <div><span class="icon-oupf_blog"  style="font-size:4.5em"></span></div>
    <div class="oupf-header-text">Blog</div>
    </div>
    </div>



<?php $the_query = new WP_Query( 'category_name=blog' ); ?>
<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

<div class="blog-post">
<a href="<?php the_permalink() ?>">
<?php

$values= wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'single-post-thumbnail' );
$thumb = '';
if(count($values) > 0) {
   $thumb = $values['0'];
}
?>

<div class="picture_and_title" style="background: url(<?php echo $thumb; ?>) no-repeat; background-size: 100%;">
    <div class="blog-post-item-content">
        <div class="date"><?php the_date(); ?></div>
        <div class="title"><?php the_title(); ?></div>

    </div>
</div>
</a>
<div class="clearfix"></div>
</div>


<?php endwhile;?>

</div>


</div>

<div class="cf"></div>
<br/>
</div>


		</div><!-- #content -->
	</div><!-- #primary -->


<?php get_footer(); ?>