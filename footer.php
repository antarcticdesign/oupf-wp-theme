<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>

	</div><!-- #main -->
	</div><!-- #page -->
<div class="cf"></div><br/><br/>



<div id="footer">



    <div id="footer_nav_wrapper">

    <div class="">
                        <img src="/wp-content/themes/oupf/assets/img/oupf-footer.svg" width="100%"/>
                    </div>

        <div class="col col_lrg">
        <img src="/wp-content/themes/oupf/assets/img/oupf_footer_logo.svg" alt="Logo" style="width: 90px"/>
        <a href="http://www.rai.ox.ac.uk/" target="_blank"><img src="/wp-content/themes/oupf/assets/img/rai_logo.svg" alt="Logo" style="width: 90px"/></a>
        <a href="http://www.ox.ac.uk" target="_blank"><img src="/wp-content/themes/oupf/assets/img/oxford_logo.svg" alt="Logo" style="width: 90px;"/></a>
        </div>

        <div class="col col_med push-left">
            <h3>Related Links</h3>
            <ul>
                <li><a href="http://www.ox.ac.uk" target="_blank">University of Oxford</a></li>
                <li><a href="http://www.rai.ox.ac.uk" target="_blank">Rothermere American Institute</a></li>
            </ul>
        </div>

        <div class="col col_med">
            <h3>Contact</h3>
            <ul>
                <li><a href="mailto:marc.maciasfauria@zoo.ox.ac.uk?subject=OUPF Query" target="_blank">Email</a></li>
                <li><a href="http://www.twitter.com/oxpolar" target="_blank">Twitter</a></li>
            </ul>
        </div>

        <div class="col col_med">
                    <h3>Account</h3>
                    <ul>
                        <li><?php wp_loginout(); ?></li>
                        <li><?php wp_register('',''); ?></li>
                    </ul>
        </div>



    </div>

</div>
                <div id="footer_fineprint" align="center">
                        © Copyright 2014-Present OUPF | Designed by <a href="http://www.antarctic-design.co.uk" target="_blank"> Antarctic Design</a>.
                    </div>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>