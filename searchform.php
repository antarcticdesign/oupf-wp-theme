<form action="/" method="get">
    <fieldset>
        <input placeholder="Search" class="oupf-search-box" type="text" name="s" id="search" value="<?php the_search_query(); ?>" />
        <input type="image" alt="Search" src="/wp-content/themes/oupf/assets/img/icons/search.svg" />
    </fieldset>
</form>