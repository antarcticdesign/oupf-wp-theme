/**
 * Created by eamonnmaguire on 11/11/14.
 */

function update_publication_options() {
    var value = $("#wp_pub_journal_type").val().toLowerCase();

    var options = [

        {"type": ["book", "book chapter"], "label": "#pubcity", "field": "#wp_pub_publisher_city"},
        {"type": ["book", "book chapter"], "label": "#publishers", "field": "#wp_pub_publishers"},
        {"type": ["book", "book chapter"], "label": "#editors", "field": "#wp_pub_editors"},
        {'type': ["book", "book chapter"], "label": "#isbn", "field":"#wp_pub_publisher_isbn"},
        {"type": ["book chapter"], "label": "#chapter_title", "field": "#wp_pub_chapter_title"}
    ];

    if(value.indexOf("book") != -1) {
        $("#venue").addClass("hidden");
        $("#wp_pub_journal_name").addClass("hidden");
    } else {
        $("#venue").removeClass("hidden");
        $("#wp_pub_journal_name").removeClass("hidden");
    }

    for (var option in options) {
        $(options[option].label).addClass("hidden");
        $(options[option].field).addClass("hidden");
    }

    for (var option in options) {

        if (options[option].type.indexOf(value) != -1) {
            $(options[option].label).removeClass("hidden");
            $(options[option].field).removeClass("hidden");
        }
    }
}