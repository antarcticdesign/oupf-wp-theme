/**
 * Created by eamonnmaguire on 12/07/2014.
 */

var OUPF = {

    active_filter: {"location": "all", "research": "all"},

    check_if_ok_to_show: function (obj) {
        var ok_to_show = true;
        for (var filter in OUPF.active_filter) {

            if (OUPF.active_filter[filter] != "all") {
                ok_to_show = $(obj).hasClass(OUPF.active_filter[filter]);
            }
            if (!ok_to_show) {
                $(obj).fadeOut(200);
                break;
            }
        }
        return ok_to_show;
    },

    filter: function (filterInputId, listId) {
        var valThis = $(filterInputId).val().toLowerCase();
        $(listId + '>div').each(function () {
            var text = $(this).text().toLowerCase().trim();
            var ok_to_show = OUPF.check_if_ok_to_show(this);
            ok_to_show ? (text.indexOf(valThis) != -1) ? $(this).fadeIn(500) : $(this).fadeOut(500) : $(this).fadeOut(200);

        });
    },

    filter_type: function (type, area, listId) {

        if (area == 'location') {
            $('.location-filter').each(function () {
                $(this).removeClass("active");
            });
        } else if (area == 'accposition') {
            $('.accposition-filter').each(function () {
                $(this).removeClass("active");
            });
        }
        else {
            $('.science-filter').each(function () {
                $(this).removeClass("active");
            });
        }

        var processed_type = type;

        if(type.indexOf("all_") >= 0) processed_type = "all";

        OUPF.active_filter[area] = processed_type;

        $("#" + type).addClass("active");

        $(listId + '> div').each(function () {
            var ok_to_show = OUPF.check_if_ok_to_show(this);
            ok_to_show ? $(this).fadeIn(500) : $(this).fadeOut(200);
        })
    }
};
