<?php
/**
 * Template Name: People Page
 *
 * Selectable from a dropdown menu on the edit page screen.
 */

get_header(); ?>

	<div class="site-content" style="width:100%">
		<div id="content" role="main">

            <script src="http://code.jquery.com/jquery-1.11.1.min.js" type="text/javascript"></script>
            <script src="/wp-content/themes/oupf/assets/js/people-filter.js" type="text/javascript"></script>

            <div class="oupf-header-people-long" style="background: url('/wp-content/themes/oupf/assets/img/header-imgs/traditional-kayak-skills.png') bottom no-repeat">

                    <div class="oupf-header-content">
                        <div><span class="icon-oupf_people_2" style="font-size:4.5em"></span></div>
                        <div class="oupf-header-text">People</div>
                    </div>

                    <div id="filters">

                        <div class="filters pull-left">

                                                                                <div id="accposition-filter">
                                                                                    <div class="col-33 filter-item">
                                                                                        <div id="all_people" class="filter-item-content accposition-filter active" onclick="OUPF.filter_type('all_people','accposition','#people')">
                                                                                            <span class="icon-arctic_antarctic"></span> All
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-33 filter-item">
                                                                                        <div id="student" class="filter-item-content accposition-filter" onclick="OUPF.filter_type('student','accposition','#people')">
                                                                                            Graduate & Visiting Students
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-33 filter-item">
                                                                                        <div id="research" class="filter-item-content accposition-filter" onclick="OUPF.filter_type('research','accposition','#people')">
                                                                                            Researchers
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="cf"></div>
                            <div id="location-filter">
                                <div class="col-33 filter-item">
                                    <div id="all_location" class="filter-item-content location-filter active" onclick="OUPF.filter_type('all_location','location','#people')">
                                        <span class="icon-arctic_antarctic"></span> All
                                    </div>
                                </div>
                                <div class="col-33 filter-item ">
                                    <div id="arctic" class="filter-item-content location-filter" onclick="OUPF.filter_type('arctic','location','#people')">
                                        <span class="icon-arctic"></span> Arctic
                                    </div>
                                </div>
                                <div class="col-33 filter-item">
                                    <div id="antarctic" class="filter-item-content location-filter" onclick="OUPF.filter_type('antarctic','location','#people')">
                                        <span class="icon-antarctic"></span> Antarctic
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div id="study-area-filter">
                                <div class="col-33 filter-item">
                                    <div id="all_science" class="filter-item-content science-filter active" onclick="OUPF.filter_type('all_science','research','#people')">
                                        <span class="icon-arctic_antarctic"></span> All
                                    </div>
                                </div>
                                <div class="col-33 filter-item">
                                    <div id="social_sciences" class="filter-item-content science-filter" onclick="OUPF.filter_type('social_sciences','research','#people')">
                                        <span class="icon-oupf_people"></span> Social Sciences and Humanities
                                    </div>
                                </div>
                                <div class="col-33 filter-item">
                                    <div id="natural_sciences" class="filter-item-content science-filter" onclick="OUPF.filter_type('natural_sciences','research','#people')">
                                        <span class="icon-oupf_natural_science"></span> Natural Sciences
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div id="name-filter" class="filter-item">
                                <div class="filter-item-content">
                                    <input id="person-filter-box" class="name-input" placeholder="Filter Researchers" name="person-name" onkeyup="OUPF.filter('#person-filter-box','#people')"/>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>



<div class="cf"></div>
<br/>
<div id="people">
<?php

$the_query = new WP_Query(array('post_type' => 'Person', 'posts_per_page' => -1, 'orderby' => 'title', 'order' =>'ASC'));

// The Loop
if ( $the_query->have_posts() ) {

	while ( $the_query->have_posts() ) {
		$the_query->the_post();?>


                    <?php
                        $research_area = strtolower(get_post_meta( get_the_ID(), 'person_info_research_area', true ));
                        $research_area = str_replace(" ", "_", $research_area);

                        $research_location = strtolower(get_post_meta( get_the_ID(), 'person_info_location_of_interest', true ));
                        $research_location = str_replace(" ", "_", $research_location);

                        $position = strtolower(get_post_meta( get_the_ID(), 'person_info_position', true ));
                        if (strpos($position,'student') !== false) {
                            $position = 'student';
                        } else {
                            $position = 'research';
                        }

                        if($research_location == "both") {
                            $research_location = "arctic antarctic";
                        }
                    ?>


                    <div id="<?php echo get_the_ID(); ?>" class="oupf-person <?php echo $research_area . " " . $research_location . " " . $position?>">
                    <?php
                        if( $research_location == "arctic antarctic") {
                            $research_location = '<span class="icon-arctic_antarctic" ></span>';
                        } else if ( $research_location == "arctic") {
                            $research_location = '<span class="icon-arctic"></span>';
                        } else {
                            $research_location = '<span class="icon-antarctic"></span>';
                        }

                        if($research_area == "natural_sciences") {
                            $research_area = '<span class="icon-oupf_natural_science"></span>';
                        } else {
                            $research_area = '<span class="icon-oupf_people"></span>';
                        }

                    ?>

                        <div class="person-image" style="background: url(<?php echo get_post_meta( get_the_ID(), 'person_info_profile_image', true );?>) no-repeat; background-size:100%" >
                            <div class="tags" style="padding: 10px">
                                <span class="tag-location icon-tag"><?php echo $research_location?></span> <span class="tag-field icon-tag"><?php echo $research_area?></span>
                            </div>
                        </div>

                        <div class="meta">
                            <div class="name"><a href="<?php echo the_permalink();?>"><?php echo get_post_meta( get_the_ID(), 'person_info_title', true );?> <?php echo the_title();?></a></div>
                            <div class="position"><?php echo get_post_meta( get_the_ID(), 'person_info_position', true );?></div>
                            <div class="department"><?php echo get_post_meta( get_the_ID(), 'person_info_department', true );?></div>
                        </div>


                    </div>


<?php
	}

} else {
	echo '<p>Sorry, no people found...</p>';
}

/* Restore original Post Data */
wp_reset_postdata();
?>
</div>
<div class="cf"></div><br/>
		</div><!-- #content -->
	</div><!-- #primary -->


<?php get_footer(); ?>