<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<div id="primary" class="site-content" style="width:100%;">
		<div id="content" role="main" >

		<div class="breadcrumbs">

		</div>
		      <div class="col-md-12">
                                  <ol class="breadcrumb" id="site-breadcrumbs">

                                  <li><a href="/people">People</a></li>
                                  <li class="active"><?php echo get_post_meta( get_the_ID(), 'person_info_title', true );?> <?php echo the_title();?></li>
                                  </ol>
                                  </div>

                                  <?php
                                    $research_area = strtolower(get_post_meta( get_the_ID(), 'person_info_research_area', true ));
                                    $research_area = str_replace(" ", "_", $research_area);

                                    $research_location = strtolower(get_post_meta( get_the_ID(), 'person_info_location_of_interest', true ));
                                    $research_location = str_replace(" ", "_", $research_location);

                                    if($research_location == "both") {
                                        $research_location = "arctic antarctic";
                                    }
                                ?>





<div class="cf"></div><br/>
            <div class="col-md-6">
            <div class="person-detail-image" style="background:url(<?php echo get_post_meta( get_the_ID(), 'person_info_profile_image', true );?>) no-repeat; background-size: 100%;">
            </div>



            </div>

            <div class="col-md-5 oupf-detail">

                            <div class="name"><?php echo get_post_meta( get_the_ID(), 'person_info_title', true );?> <?php echo the_title();?></div>

                            <div class="tags" style="padding-bottom: 20px; padding-top: 10px; border-bottom: 1px solid #f1f2f1;"><span class="tag-location bigger-tag"><?php echo $research_location?></span> <span class="tag-field bigger-tag"><?php echo str_replace("_", " ",$research_area)?></span>
                                            </div>


                                            <div class="position"><?php echo get_post_meta( get_the_ID(), 'person_info_position', true );?></div>
                                            <div class="department"><?php echo get_post_meta( get_the_ID(), 'person_info_department', true );?></div>
                                             <?php
                                                 if( $research_location == "arctic antarctic") {
                                                    $research_location = str_replace(" ", " & ", $research_location);
                                                 }
                                             ?>



                        <div class="person-bio">
                                                <div class="clearfix"></div><br/>
                                                <div class="cf"></div>

                        <div style="font-family: 'Foustbdo';">Polar research interests</div><br/>
                        <?php echo get_post_meta( get_the_ID(), 'person_info_biography', true );?>

                        </div>

                        <div class="contact-details">
                                    <table style="table-layout:fixed">
                                    <?php
                                        if (get_post_meta( get_the_ID(), 'person_info_email', true ) != "") {
                                    ?>
                                        <tr><td style="width: 30%">Email</td> <td style="width: 70%"><a href="mailto:<?php echo get_post_meta( get_the_ID(), 'person_info_email', true )?>"><?php echo get_post_meta( get_the_ID(), 'person_info_email', true )?></a></td>

                                    <?php
                                        }
                                    ?>

                                    <?php
                                        if (get_post_meta( get_the_ID(), 'person_info_website', true ) != "") {
                                    ?>
                                        <tr><td style="width: 30%">Website</td> <td style="width: 70%"><a target="_blank" style="width: 200px; overflow: scroll;" href="<?php echo get_post_meta( get_the_ID(), 'person_info_website', true )?>"><?php echo substr(get_post_meta( get_the_ID(), 'person_info_website', true ), 0, 50)?></a></td>
                                    <?php
                                        }
                                    ?>
                                    </table>
                                    </div>
                    </div>

             </div>



               <div class="cf"></div>
               <br/><br/>
               <?php edit_post_link( __( 'Edit Person', 'person' ), '<span class="edit-link">', '</span>' ); ?>
               <div class="cf"></div>
               <br/><br/>


		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>