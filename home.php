<?php
/**
 * Template Name: Home Page
 *
 * Selectable from a dropdown menu on the edit page screen.
 */

get_header(); ?>

	<div class="site-content" style="width:100%">
		<div id="content" role="main">
            <div class="oupf-header">
                <div class="oupf-header-content">
                    <span class="icon-oupf_small" style="font-size: 7.7em"></span><br/>
                    <span class="mission-text">Bringing Together Arctic and Antarctic Researchers in Oxford University</span>
                </div>
            </div>

            <div class="tweet-container" >
            <div id="twitter_icon" class="pull-left">
                <img src="/wp-content/themes/oupf/assets/img/icons/twitter.svg" width="40px"/>
            </div>



            <div class="tweet pull-left" id="twitter"><div class="spinner">
                                                        <div class="rect1"></div>
                                                        <div class="rect2"></div>
                                                        <div class="rect3"></div>
                                                        <div class="rect4"></div>
                                                        <div class="rect5"></div>
                                                      </div></div>
            <div class="cf"></div>
            </div>


<div class="latest-oupf-posts">
<h3>Latest News</h3>

<?php
$args = array(
	'showposts' => '4',
	'category_name' => 'blog'
);
?>
<?php $the_query = new WP_Query( $args ); ?>
<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

<div class="blog-post">
<a href="<?php the_permalink() ?>">
<?php

$values= wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'single-post-thumbnail' );
$thumb = '';
if(count($values) > 0) {
   $thumb = $values['0'];
}
?>

<div class="picture_and_title" style="background: url(<?php echo $thumb; ?>) no-repeat; background-size: 100%;">
    <div class="blog-post-item-content">
    <div class="date"><?php the_date(); ?></div>
        <div class="title"><?php the_title(); ?></div>

    </div>
</div>
</a>
</div>

<?php endwhile;?>

</div>

<div class="cf"></div>


<div class="upcoming-oupf-events">
<h3><span class="ico-event-calendar"></span> Upcoming Events</h3>
<?php
if (class_exists('EM_Events')) {


    $event_format = '<div class="event-item">'.
    '<div class="event-name">#_EVENTLINK</div>'.
    '<div class="event-excerpt">#_EVENTEXCERPT{20,...}</div>'.
    '<div class="event-date"><span class="ico-event-clock" style="margin-right: 4px"></span> #_EVENTDATES - <span class="event-time">#_EVENTTIMES</span></div>'.
    '<div class="event-location"><span class="ico-event-location" style="margin-right: 4px"></span> #_LOCATIONLINK</div>'.
    '</div>';
    echo EM_Events::output( array('limit'=>4,'orderby'=>'event_start_date', 'format'=>$event_format) );
}
?>
<div class="cf"></div>

</div>
<div class="cf"></div>



		</div><!-- #content -->
	</div><!-- #primary -->

<link rel="stylesheet" href="/wp-content/themes/oupf/assets/css/oupficons.css" type="text/css"/>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="/wp-content/themes/oupf/assets/js/jquery.twitter.min.js"></script>

<script type="text/javascript">

    twitterFetcher.fetch('539493393731424256', 'twitter', 1, true, false, false);


</script>
<?php get_footer(); ?>